\section{Introduction}


The code city metaphor is probably the most successful visualization for source code metrics in a 3D environment. It was proposed for the first time in \cite{codecity}, which described CodeCity, its first implementation. The original CodeCity creates cities that look real, due to the combination of layouts, topologies, and metric mappings applied at an appropriate level of granularity. It shows object-oriented software systems as cities that can be intuitively explored \cite{ref_article4}, by mapping metrics to features (height, size, color) of the buildings, and placing those buildings in locations related to their position in the object hierarchy. The city metaphor offers a clear notion of locality, supporting orientation, and does not hide the underlying structural complexity that cannot be oversimplified. The original CodeCity has been revisited and extended by several teams~\cite{gocity}, \cite{ref_vr_city} or \cite{ref_vr_city_2}.

Given this popularity of the code city metaphor, we decided to build our own implementation of it, using some technologies recently available in web browsers. These technologies, such as WebVR/WebXR and WebGL allow for the development of 3D, VR-ready applications for the browser, thus being multiplatform and easy to integrate with other front-end modules, and Web APIs. Our main intention was to show that something similar to the original CodeCity can be implemented with relative ease, so that it can run in major web browsers, including those of virtual reality (VR) devices, with little or no performance constraints.

During the last years, several reusable toolsets for the retrieval and analysis of data from software development repositories have also emerged. We were also interested in showing how they can be used to easily build pipelines capable of producing the kind of data needed by CodeCity visualizations. This way, data retrieval and analysis would be decoupled from data visualization, allowing for a more flexible approach that would allow for much more flexibility in the metrics that can be visualized, and the extension of the CodeCity visualizations to new realms.

Formalizing our objectives as research questions, they are:

\begin{itemize}
\item RQ1: Can the visualization of CodeCity be implemented as a front-end web app, using already available modules for WebVR/WebXR, WebGL, and related technologies?
\item RQ2: Can a complete pipeline be produced, to feed the app in RQ1 with data from any git repository, mainly using already available tools?
\end{itemize}

In the process of answering these questions, we reproduce the original work on CodeCity, introducing some differences with the original:

\begin{itemize}
\item We use two completely decoupled tools: a web app for the visualization, and a retrieval-analysis pipeline for producing the data to be visualized. Both will be composed by defining a JSON-based data format.
\item Thanks to this decoupling, our tools are very flexible on the metrics to visualize, and even on the nature of the entities that will be visualized (classes, files, complete repositories, etc).
\item With almost no extra effort, the visualization tool is portable to many platforms, including VR headsets, where it works producing an immersive VR scene.
\item In the area of functionality, our implementation allows for the navigation of the evolution history of metrics over time, with a high level of interaction with the visualized data. Snapshots of the history can be visualized in several kinds of intervals, from a granularity of single commits, to periodic (weekly, monthly, yearly) snapshots.
\end{itemize}

We also obtain some user feedback about the resulting implementation, via a survey to developers, some of them already familiar with the visualized software, some others not. This allows us to check to which extent the resulting visualizations, and the capabilities for interacting with the history of source code, help them to better understand code evolution. The feedback obtained is interesting both from the specific point of view of our implementation, but also from the point of view of the code city metaphor.

All the software we show in this paper is available as a part of the BabiaXR data visualization toolset, or in the reproduction package for the paper. We expect that its availability will also help to lower the barriers for other researchers willing to explore extensions of CodeCity, specially in VR environments.

The structure of the paper is as follows. In the next section, we present the state of the art and some related work. Then, in Section~\ref{sec:implementation}, we show the details of our implementation (both of the data analysis pipeline, and of the web app for visualization, BabiaXR CodeCity). After that, in Section~\ref{sec:scenarios} we show how we use BabiaXR CodeCity for analyzing the code in two different cases. For one of those cases, show in~\ref{sec:feedback} the user feedback we obtained. Finally, we present some discussion of the results in Section~\ref{sec:discussion}, some conclusions (Section~\ref{sec:conclusions}), and details about the reproduction package and acknowledgments (Section~\ref{acks}).
% FIXME: add labels for sections
% FIXME: add reference to the reproduction package in the acks section, and extend the name of the section to include reproduction package.

