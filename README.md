# Time evolving cities in a web environment

## Visit the reproduction package here

- [repr_package folder](./repr_package/README.md)
- [Cities analized live page](https://thesis-dlumbrer.gitlab.io/vissoft2020)

## Video of the demonstration

https://youtu.be/e1jYAEtC1wY