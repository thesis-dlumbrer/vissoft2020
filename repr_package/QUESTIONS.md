# BabiaXR Code City Experiment

## Instructions

If you are in this form is because you were invited to test the approach of the time evolution code city of the Perceval (https://github.com/chaoss/grimoirelab-perceval) project. The following links will take you to the demos, but first, please finish reading this information before going there:

[1]. https://thesis-dlumbrer.gitlab.io/vissoft2020/examples/codecityjs/time_evolution_perceval_months/
[2]. https://thesis-dlumbrer.gitlab.io/vissoft2020/examples/codecityjs/time_evolution_perceval_months_inverse/


Once inside the links, you will see 2 important elements on there, the city of the code, and the navigation bar. 

The navigation bar represents as red dots, the different time snapshots that have been analyzed, in this case, one red dot per month, this means that the city will “evolve” automatically (by default) along with these red dots, showing the information of the city month by month. Each red dot has a legend showing the date of the time snapshot selected with the corresponding commit state of the code, the red dot on the far right represents the latest time snapshot, and the red dot on the far left represents the first time snapshot. In terms of interaction, there are buttons that change the behavior of the time evolution, you can do these actions:

- Click on the red dots to move between time snapshots, a legend on top of the dot will show the current snapshot and commit

- Click on the pause/play button to pause or resume the time evolution

- Click on the Next/Previous button to move one-time snapshot forward/backward, this will stop the time evolution

- Click on the Go Forward/Go Backward button to change the direction of the time evolution.


The city represents the code of the project Perceval, it will evolve changing the area/height of the buildings following the time snapshots of the navigation bar. NOTE that buildings may appear/disappear. Each building represents a file of the code, each quarter and the quarter level represents the folder tree that file belongs to. The height and the area of each building represents a metric of the file, and each demo uses different metrics for it:

- The demo of the link [1] uses the following metrics: The height of each building represents the lines of code that the file has, if the file does not have lines of code, it will have the minimum value, in this case, 0.1. The area of the base of each building represents the number of functions that the file has, if the file does not have any function, it will have an area value of 1.

- The demo of the link [2] uses the following metrics: The height of each building represents the number of functions that the file has, if the file does not have functions, it will have the minimum value, in this case, 0.5 of area. The area of the base of each building represents the lines of code that the file has, the minimum value for the base is 1.


The city is evolving automatically along the time, so the buildings may change their area/height each time snapshot. In terms of interaction with the city, you can use your cursor to do these actions:

-Move the cursor above a building to show a legend with what it represents on top of it, leave the cursor and the title will disappear.

- Click on a building to fix the legend, click again to remove it.

- Click on a quarter base to show a transparent box with a legend showing what it represents on top of it, click again to remove the legend and the transparent box.


Moreover, there are three circle icons with an (i) of information that will show a popup directly in the scene with this information summarized, please, check them when you want if you have any doubt doing the experiment. You can close the information popup clicking on the X icon.


To finish, please, before starting the experiment, take a look at the questions below and measure how long it took you to do this test.


I really appreciate the time that you dedicated to this test, thank you very much.

## Questions

1. How much do you know about the Perceval project? (1 - 5)
2. What represents the city?
3. What represents a building?
4. What represents a quarter?
5. At what time, month, or time point, did the code change the most?
6. What do you tell me about the directory “perceval/backends”?
7. What do you tell me about the file “perceval/backends/core/gitter.py”?
8. What do you tell me about the file “tests/test_github.py”?
9. What do you tell me about the file “tests/data/twitter/tweets.json”?
10. What do you tell me about the directory “tests/”?
11. Do you think that using this visualization helped you to understand better the questions? How/Why? You can compare it analyzing directly the repository (https://github.com/chaoss/grimoirelab-perceval)
12. How long did it take you to do this experiment? (in minutes)
13. Have you used VR glasses for the experiment? (if yes, specify which one)
14. What browser did you use for the experiment? (Chrome, Firefox, ...)
15. Please, give any feedback about this approach tool (personal opinions, improvements, if it was useful…)