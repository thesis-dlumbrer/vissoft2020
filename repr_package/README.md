# Reproduction package

This folder has all the needed files for the reproduction package.

## Launch the examples

For seeing the cities analyzed in the paper, please, launch an HTTP server in this directory, using python, node, etc. :

```
python -m simpleHttpServer

//or

http-server
```

Or you can simply visit the next page:

https://thesis-dlumbrer.gitlab.io/vissoft2020


## Survey

The survey instructions and questions are in the file [QUESTIONS.md](./QUESTIONS.md)